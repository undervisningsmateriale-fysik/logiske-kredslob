\backgroundsetup{
position=current page.north east,
vshift=-35pt,
hshift=-200pt,
angle=0,
scale=0.3,	
opacity=0.2,
contents={\includegraphics{my-by-sa.png}}
}
\BgThispage

\maketitle


\section{Introduktion til logiske kredsløb}
Vi skal i dette arbejdsark undersøge nogle grundlæggende logiske kredsløb og se hvordan man ved hjælp af disse logiske kredsløb kan bygge et kredsløb der kan fungere som en elektronisk hukommelse, som den der sidder i din computer.  

\begin{formelboks}
\textbf{Vigtigt faktum 1:} hvis der ikke løber en strøm gennem en resistor er spændingen den samme på begge sider af resistoren.
\end{formelboks}

At ovenstående faktum er korrekt kan vi overbevise os om, ved at kigge på Ohms lov. For en resistor er spændingsfaldet proportionalt med strømmen gennem resistoren, dvs. hvis strømmen er nul er spændingsfaldet også nul. Hvis der kun er meget få ladninger som skal passere resistoren koster det altså ikke ladningerne ret meget energi, kun hvis mange ladninger prøver at komme gennem resistoren på samme tid, så kræver det meget energi at passere resistoren. 


\begin{opg} 
Overbevis dig om rigtigheden af ovenstående vigtige faktum ved at måle på kredsløbet i figur \vref{fig:not-gate-simple-switch}. Lad \( R_{1} = 10^{6} \si{Ohm} \).
\end{opg}


\begin{figure}[htbp]
  \begin{center}
	\input{grafik/ellaere/tikz/not-gate-simple-switch.tex}
    \caption{Spændingen er den samme på begge sider af resistoren, sålænge kredsløber ikke er sluttet. Så snart knappen trykkes ned så der kan løbe en strøm, så vil der være et spændingsfald over resistoren.}
    \label{fig:not-gate-simple-switch}
  \end{center}
\end{figure}




\begin{formelboks}
\textbf{Vigtigt faktum 2:} output ledningen kan ikke altid (og bør aldrig) bruges til at levere strøm. Den logiske gate skal kun stå for logikken, \textbf{ikke} for at levere strøm til fx at drive en lyskilde eller en motor.
\end{formelboks}


\begin{opg} 
Prøv at forbinde pluspolen af en diode direkte til out-porten fra kredsløbet i forrige opgave (se figur \vref{fig:out-to-diode}). Du kan nu observere at dioden kun lyser meget svagt. (Kan du også gætte hvorfor den kun lyser svagt)?
\end{opg} 

\begin{figure}[htbp]
  \begin{center}
	\input{./grafik/ellaere/tikz/diode-connected-to-out.tex}
    \caption{Spændingen er den samme på begge sider af resistoren, sålænge kredsløber ikke er sluttet. Så snart knappen trykkes ned så der kan løbe en strøm, så vil der være et spændingsfald over resistoren.}
    \label{fig:out-to-diode}
  \end{center}
\end{figure}


\section{Funktioner af 1 variabel: NOT}

En funktion kan ses som en maskine der tager et input og leverer et output. Et logiske kredsløb som i figur \vref{fig:not-gate-simple-switch} kan modtage en input-spænding og leverer herefter en output-spænding.

Funktioner kan opskrives på tabelform hvor man for et givent input kan se hvad funktionen giver som output. For reelle funktioner er det imidlertid meget upraktisk at opskrive de uendeligt mange input- og tilhørende outputværdier.

Derfor opskriver man oftest funktionsforskriften når man vil forklare hvad en (reel) funktion gør ved et givent input.


\begin{marginfigure}
\centering
\includegraphics[width=\linewidth]{./grafik/ellaere/py/function-of-1variable-parabola.png}
\caption{En funktion af variablen \(x\).}
\label{marginfig:function-1variable-parabola}
\end{marginfigure}


\begin{eks}
Her ses et simpelt eksempel på en funktion af 1 variabel.

\[ f(x) = x^{2} + 1 \]  

funktionens navn er \(f\) og variablens navn er \(x\). For at angive at værdien af \(f\) afhænger af hvilket \(x\) man har valgt, så skriver man \(f(x)\). Funktionsforskriften ovenfor fortæller hvordan man beregner værdien af \(f\) for et givet \(x\). Hvis for eksempel \(x\) vælges til 3, så beregner man funktionen  \(f\) i punktet \(x=3\) på følgende måde:
 
\[ f(3) = 3^{2} + 1  = 10 \]

Og hvis man beregner f for en lang række af x-værdier så kan man indtegne grafen for funktionen i et almindeligt koordinatsystem. Figur \ref{marginfig:function-1variable-parabola} viser grafen for \( f(x) = x^2 + 1 \).
\end{eks}


For funktioner over binære tal er det meget nemt at opskrive funktionen på tabelform fordi der kun er to mulige input værdier: 1 og 0.
Til gengæld får man ikke meget ud at af tegne grafen for funktionen i et koordinatsystem.  

\begin{eks}
En funktion over de binære tal repræsenteret ved sin tabel.

\begin{center}
\begin{tabular}{|c|c|} 
\toprule
In & Out \\ \midrule
0 & 1 \\
1 & 0 \\ \bottomrule
\end{tabular}
\label{tab:not-function}
\end{center}

Denne funktion kaldes for en NOT funktion eller en inverter fordi 1 bliver til 0 og 0 bliver til 1.  
\end{eks}

\begin{opg} 
Byg et kredsløb der virker som en NOT funktion. Du skal bruge en resistor, en trykknap og en lysdiodeindikator. Med trykknappen kan du styre om inputværdien er 0 eller 1. Lysdiodeindikatoren skal du benytte til at tjekke om kredsløbet opfører sig korrekt.
\end{opg} 

\begin{marginfigure}
\centering
\input{./grafik/ellaere/tikz/not.tex}
\caption{Symbolet for et NOT kredsløb}
\label{fig:not-symbol}
\end{marginfigure}



\section{Funktioner af 2 variable: AND og OR}
Hvis en funktion \(f\) afhænger af 2 variable \(x\) og \(y\) skriver man \( f(x,y)\). Det betyder at man både skal vælge et \(x\) og et \(y\) for at kunne beregne \(f\).

\begin{eks}
Her ses et simpelt eksempel på en funktion af 2 reelle variable \(x\) og \(y\).

\[ f(x,y) = x^{2} + y \]  

Hvis for eksempel \(x\) vælges til 3, og \(y\) vælges til 2, så beregner man funktionen \(f\) i punktet \((3,2)\) (dvs. \(x=3\) og \(y=2\)) på følgende måde:

\[ f(3,2) = 3^{2} + 2 = 11 \]  

Og hvis man beregner \(f\) for en lang række af x- og y-værdier, så kan man indtegne grafen for funktionen i et 3 dimensionelt koordinatsystem. Figur \vref{fig:function-2variables-parabola} viser grafen for \( f(x,y) = x^{2} + y \).
\end{eks}


\begin{marginfigure}
\centering
\includegraphics[width=\linewidth]{./grafik/ellaere/py/function-of-2variables-parabola.png}
\caption{En funktion af to variable \(x\) og \(y\).}
\label{fig:function-2variables-parabola}
\end{marginfigure}

\begin{eks}
\label{eks:and-funktion}

En funktion der afhænger af 2 binære variable kan ligeledes let repræsenteres ved en tabel.

\begin{center}
\begin{tabular}{|c|c|c|} 
\toprule
a & b & Out \\ \midrule
1 & 1 & 1 \\
1 & 0 & 0 \\
0 & 1 & 0 \\
0 & 0 & 0 \\ \bottomrule
\end{tabular}
\end{center}

variablen \(a\) repræsenterer det ene input, og variablen \(b\) repræsenterer det andet input. 

\end{eks}



\begin{opg} 
Byg et kredsløb der virker som en AND funktion. Tabellen i eksempel \ref{eks:and-funktion} fortæller hvordan AND-funktionen skal virke.
\end{opg} 

\begin{marginfigure}
\centering
\input{./grafik/ellaere/tikz/and.tex}
\caption{Symbolet for et AND kredsløb}
\label{fig:and-symbol}
\end{marginfigure}


\begin{opg} 
Byg et kredsløb der virker som en OR funktion. Nedenstående tabel fortæller hvordan OR-funktionen skal virke.

\begin{center}
\begin{tabular}{|c|c|c|} 
\toprule
a & b & Out \\ \midrule
1 & 1 & 1 \\
1 & 0 & 1 \\
0 & 1 & 1 \\
0 & 0 & 0 \\ \bottomrule
\end{tabular}
\label{tab:or-funktion}
\end{center}
\end{opg} 

NOT, AND og OR kredsløb er grundlaget for al elektronik, og en computer kan faktisk opbygges ved at sætte tilstrækkeligt mange NOT, AND og OR kredsløb sammen på den rigtige måde!


\begin{marginfigure}
\centering
\input{./grafik/ellaere/tikz/or.tex}
\caption{Symbolet for et OR kredsløb}
\label{fig:or-symbol}
\end{marginfigure}



\section{Ekstraopgaver til de hurtige: NAND og XOR}
Udover NOT, AND og OR, findes der yderligere 2 grundlæggende logiske kredsløb som er meget anvendte; NAND og XOR kredsløbene. 

\begin{opg} 
Byg et kredsløb der virker som en NAND funktion. 

\begin{center}
\begin{tabular}{|c|c|c|} 
\toprule
a & b & Out \\ \midrule
1 & 1 & 0 \\
1 & 0 & 1 \\
0 & 1 & 1 \\
0 & 0 & 1 \\ \bottomrule
\end{tabular}
\label{tab:nand-funktion}
\end{center}


Det viser sig faktisk at en computer kan opbygges ved udelukkende at sætte tilstrækkeligt mange NAND kredsløb sammen på den rigtige måde! Det kan man fordi, man ved at sammensætte NAND kredsløb kan bygge både NOT, AND or OR kredsløb.  
\end{opg} 

\begin{marginfigure}
\centering
\input{./grafik/ellaere/tikz/nand.tex}
\caption{Symbolet for et NAND kredsløb}
\label{fig:nand-symbol}
\end{marginfigure}


\begin{opg}
Forklar hvordan et NAND kredsløb kan bygges ud fra de kredsløb som du tidligere har bygget. (Hint: tænk over hvorfor det hedder et NAND kredsløb).
\end{opg} 




\begin{opg}
Lav en skitse af et kredsløb der virker som en XOR funktion. Benyt enten 4 NAND funktioner, eller 1 NAND, 1 AND og 1 OR funktion. Nedenstående tabel viser hvordan en XOR funktionen skal fungere.   

\begin{center}
\begin{tabular}{|c|c|c|} 
\toprule
a & b & Out \\ \midrule
1 & 1 & 0 \\
1 & 0 & 1 \\
0 & 1 & 1 \\
0 & 0 & 0 \\ \bottomrule
\end{tabular}
\label{tab:xor-funktion}
\end{center}


\end{opg} 

\begin{marginfigure}
\centering
\input{./grafik/ellaere/tikz/xor.tex}
\caption{Symbolet for et XOR kredsløb}
\label{fig:xor-symbol}
\end{marginfigure}


\begin{opg} 
Forklar hvorledes XOR adskiller sig fra OR. 
\end{opg}
 

\section{Transistoren som kontakt}

I stedet for manuelt at slutte kredsløbet ved at trykke på en knap, som vi har gjort indtil videre, så kan man bruge en transistor. En transistor kan ses som en elektronisk kontakt der kan åbne og lukke for strømmen når den bliver bedt om det.

\begin{marginfigure}
\centering
\includegraphics[width=0.8\linewidth]{./grafik/ellaere/svg/transistor-image.png}
\caption{BC547 transistor.}
\label{marginfig:bc547}
\end{marginfigure}

På figur \vref{marginfig:bc547} ses en transistor af typen BC547 som er den type vi skal bruge i dette arbejdsark. Transistoren har, som du kan se, tre indgange. Indgang C (collector) skal altid forbindes til pluspolen, Indgang E (emitter) skal altid forbindes til den negative pol, og indgang B (base) er den indgang som styrer om transistoren er åben eller lukket, dvs. om der kan løbe en strøm fra C til E eller ej. Hvis spændingen på B er større end 1V, så er transitoren åben, og hvis spændingen på B er mindre end 0.6V, så er transistoren lukket.


Figur \vref{marginfig:bc547-symbol} viser hvordan man repræsenterer en BC547 transistor symbolsk i et kredsløbsdiagram.

\begin{marginfigure}
\centering
\includegraphics[width=0.7\linewidth]{./grafik/ellaere/svg/npn-transistor-symbol.png}
\caption{Symbolet for en transistor af typen BC547.}
\label{marginfig:bc547-symbol}
\end{marginfigure}


\begin{opg}  
Karakteristik af transistor. \newline

\begin{description}
\item[a)] Forbind transistoren som vist på figur \vref{fig:transistor-as-switch}. Vælg resistoren \(R_{1}\) til at være fra \(100-300 \si{Ohm} \), og vælg resistoren \(R_{2}\) til at være omkring \(5000\) \si{ohm}. Tjek at du kan variere input-spændingen til transistoren, ved at ændre på den variable modstand. 

\item[b)] Benyt LoggerPro med tilhørende voltmeter og amperemeter til at måle sammenhørende værdier for spændingen på indgang B, og strømmen gennem transistoren fra C til E. 


\item[c)] Lav en (U,I)-graf for transistoren. 
\end{description}
\end{opg} 



\begin{figure}[htbp]
  \begin{center}
	\input{./grafik/ellaere/tikz/transistor-as-switch.tex}
	\setfloatalignment{b}
    \caption{Kredsløb beregnet til at lave en (U,I)-karakteristik af transistoren.}
    \label{fig:transistor-as-switch}
  \end{center}
\end{figure}



\begin{opg} 
Benyt din målinger fra forrige opgave til at argumentere for ved hvilke spændinger transistoren er åben, og ved hvilke spændinger transistoren er lukket.  
\end{opg} 


\begin{opg}
Test af NOT kredsløb med transistor. \newline

\begin{description}
\item[a)] Byg et NOT kredsløb, nu med en transistor i stedet for en trykknap.\newline
NB: for at undgå at der løber strøm gennem transistoren fra B til E, er det en god ide at sætte en \(5000 \si{Ohm}\) resistor mellem NOT kredsløbets input og transistorens B indgang, som vist på figur \vref{fig:resistor-before-base}.   

\item[b)] Benyt kredsløbet på figur \vref{fig:test-af-notgate} til at måle hvordan NOT kredsløbets output-spænding afhænger af input-spændingen (trekanten repræsenterer det NOT kredsløb du lige har bygget i forrige delopgave).

\item[c)] Lav en graf der viser kredsløbets output-spænding som funktion af input-spændingen.
\end{description} 
\end{opg} 


\begin{marginfigure}
\centering
\input{./grafik/ellaere/tikz/resistor-to-npn-base.tex}
\setfloatalignment{t}
\caption{En resistor mellem NOT kredsløbets input og transistorens B indgang, sikrer at der ikke løber en (særlig stor) strøm fra B til E.}
\label{fig:resistor-before-base}
\end{marginfigure}


\begin{figure}[htbp]
  \begin{center}
	\input{./grafik/ellaere/tikz/not-out-vs-in.tex}
	\setfloatalignment{t}
    \caption{Kredsløb til at måle output-spændingen som funktion af input-spændingen.}
    \label{fig:test-af-notgate}
  \end{center}
\end{figure}



\section{FLIP/FLOP (1-bit hukommelse)}

\begin{opg} 
Byg to NOT kredsløb på det samme breadboard, og tjek at de begge virker som de skal.
\end{opg} 

\begin{opg}
Forbind nu de to NOT kredsløb som vist på figur \vref{fig:flip-flop-not}. Dette kredsløb kaldes for en flip/flop og virker som en elektronisk hukommelse hvor man kan gemme enten 0 eller 1.

Bemærk at der en en >>løs<< ledning som kan bruges til, at læse fra, eller skrive til hukommelsen. 
\end{opg} 

\begin{figure}[htbp]
  \begin{center}
	\input{./grafik/ellaere/tikz/flip-flop-not.tex}
	\setfloatalignment{t}
    \caption{Flip/flop bestående af to NOT kredsløb.}
    \label{fig:flip-flop-not}
  \end{center}
\end{figure}



\begin{opg}
For at få et visuelt indtryk af om din elektroniske hukommelse virker, kan du prøve at lave kredsløbet på figur \vref{fig:visuel-test-af-flip-flop}. Skriv til hukommelsen ved at sætte den løse ledning til enten 0 eller 5 volt. Læs fra hukommelsen ved at forbinde den løse ledning til transistorens base (B-indgang).
\end{opg}

\begin{figure}[htbp]
  \begin{center}
	\input{./grafik/ellaere/tikz/visual-test-flip-flop.tex}
	\setfloatalignment{t}
    \caption{Kredsløb til visuel test af den elektroniske hukommelse.}
    \label{fig:visuel-test-af-flip-flop}
  \end{center}
\end{figure}


\begin{opg} 
Hvor mange flip/flops skal du lave for at få 1 GB hukommelse. Husk at 1 GB svarer til 8 Gbit. 
\end{opg} 

En computer har to forskellige slags hukommelse. Harddisken har hukommelse der kan huske data selvom computeren er slukket. Herudover har din computer RAM (Random Acces Memory) som er en hukommelse der er meget hurtigere at læse/skrive til, men som til gengæld glemmer data når computeren slukkes.

\begin{opg} 
Hvilken slags hukommelse har du lige bygget?  
\end{opg} 